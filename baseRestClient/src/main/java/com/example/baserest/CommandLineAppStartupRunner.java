package com.example.baserest;

import com.example.baserest.models.Equipe;
import com.example.baserest.models.UserCredentials;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;


@Component
public class CommandLineAppStartupRunner implements CommandLineRunner {
    private final RestTemplate restTemplate;
    private final String serverBaseURI;

    public CommandLineAppStartupRunner(RestTemplate restTemplate, @Value("${uri.du.serveur.rest}") String serverBaseURI) {
        this.restTemplate = restTemplate;
        this.serverBaseURI = serverBaseURI;
    }

    @Override
    public void run(String...args)  {
        var userCredentials = new UserCredentials();
        userCredentials.setIdentifiant("Admin");
        userCredentials.setMdp("azerty");
        String jwtToken = restTemplate.postForObject(this.serverBaseURI + "/login", userCredentials, String.class);
        if (jwtToken == null || jwtToken.length() == 0) {
            throw new IllegalStateException("JWT invalide!");
        }
        var headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setBearerAuth(jwtToken);
        HttpEntity<Void> entity = new HttpEntity<>( headers);
        ResponseEntity<Equipe[]> equipesResponse =
                restTemplate.exchange(this.serverBaseURI + "/equipes", HttpMethod.GET,entity, Equipe[].class);
        if(equipesResponse.getStatusCode() == HttpStatus.OK) {
            System.out.println("Réponse reçu:");
            System.out.println(equipesResponse);
        } else {
            System.out.println("Erreur http : " + equipesResponse.getStatusCodeValue());
        }
    }
}