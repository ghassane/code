package jackson;


import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class Main {
    public static void main(String[] args) throws IOException {
        ObjetASerialiser objetASerialiser1 = new ObjetASerialiser(1, "test");
        ObjetASerialiser objetASerialiser2 = new ObjetASerialiser(2, "test2");
        try (FileOutputStream fos = new FileOutputStream("./MonObjetOctets")) {
            try (ObjectOutputStream oos = new ObjectOutputStream(fos)){
                oos.writeInt(9000);
                oos.writeObject(objetASerialiser1);
                oos.writeBoolean(false);
                oos.writeFloat(78.9f);
                oos.writeObject(objetASerialiser2);
                oos.writeObject("Toto");
            }
        }
    }
}


