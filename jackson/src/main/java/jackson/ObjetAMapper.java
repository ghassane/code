package jackson;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "nom", "valid", "age"})
public final class ObjetAMapper {
    @JsonIgnore
    private final int id;
    @JsonAlias({"NOM", "le_nom", "o_nom"})
    private final String nom;
    private final boolean valide;
    @JsonProperty("o_age")
    private final int age;

    public ObjetAMapper(int id, String nom, boolean valide, int age) {
        this.id = id;
        this.nom = nom;
        this.valide = valide;
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public boolean isValide() {
        return valide;
    }

    public int getAge() {
        return age;
    }
}




