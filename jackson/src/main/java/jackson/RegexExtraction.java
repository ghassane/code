package jackson;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexExtraction {
    public static void main(String[] args) {
        Pattern motifValidationEmail = Pattern.compile("(?<nom>.+)@(?<host>.+)\\.(?<domaine>[a-z]{2,}+)");
        Matcher matcher = motifValidationEmail.matcher("abc@toto.fr");
        if(matcher.find()) {
            System.out.println("nom: " + matcher.group("nom"));
            System.out.println("host: " + matcher.group("host"));
            System.out.println("domaine: " + matcher.group("domaine"));
        }
    }
}

