package jackson;


import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class Main2 {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        try (FileInputStream fis = new FileInputStream("./MonObjetOctets")) {
            try (ObjectInputStream ois = new ObjectInputStream(fis)){
                System.out.println(ois.readInt());
                ObjetASerialiser objetASerialiser1 = (ObjetASerialiser) ois.readObject();
                System.out.println("O1.a = " + objetASerialiser1.getA());
                System.out.println("O1.b = " + objetASerialiser1.getB());
                System.out.println(ois.readBoolean());
                System.out.println(ois.readFloat());
                ObjetASerialiser objetASerialiser2 = (ObjetASerialiser) ois.readObject();
                System.out.println("O2.a = " + objetASerialiser2.getA());
                System.out.println("O2.b = " + objetASerialiser2.getB());
                System.out.println(ois.readObject());
            }
        }
    }
}




