package jackson;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexExemple {
    public static void main(String[] args) {
        Pattern motifValidationEmail = Pattern.compile(".+@.+\\.[a-z]{2,}+");
        validerEmail(motifValidationEmail, "abc@toto.fr");
        validerEmail(motifValidationEmail, "@toto.fr");
        validerEmail(motifValidationEmail, "abc@toto");
        validerEmail(motifValidationEmail, "abctoto.fr");
        validerEmail(motifValidationEmail, "abc@totofr");
        validerEmail(motifValidationEmail, "05258@8889.31313");
        validerEmail(motifValidationEmail, "05258@8889.com");
    }

    private static void validerEmail(Pattern motifValidationEmail, String email) {
        Matcher matcher = motifValidationEmail.matcher(email);
        if(matcher.matches()) {
            System.out.println("Email valide!");
        } else {
            System.out.println("Email invalide!");
        }
    }
}

