package jackson;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
public final class ObjetASerialiser implements Serializable {
    private final static long serialVersionUID = 12L;
    private int a;
    private String b;
    public ObjetASerialiser(int a, String b) {
        this.a = a;
        this.b = b;
    }
    public int getA() { return a; }
    public String getB() { return b; }
    private void writeObject(ObjectOutputStream oos) throws IOException {
        oos.writeInt(a);
        oos.writeObject(b);
    }
    private void readObject(ObjectInputStream ois) throws ClassNotFoundException, IOException {
        a = ois.readInt();
        b = (String) ois.readObject();
    }
}




