package com.example.baserest.di;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MaConfiguration {
    @Bean
    public Moteur creeMoteur() {
        return new Moteur();
    }
    @Bean
    public Voiture creeVoiture() {
        return new Voiture();
    }
}
