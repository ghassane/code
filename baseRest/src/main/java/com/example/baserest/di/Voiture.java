package com.example.baserest.di;

import org.springframework.beans.factory.annotation.Autowired;

public class Voiture {
    private Moteur moteur;
    @Autowired
    public Voiture(Moteur moteur) {
        this.moteur = moteur;
    }
    public void avancer() {
        moteur.demarrage();
        //...
    }
}

