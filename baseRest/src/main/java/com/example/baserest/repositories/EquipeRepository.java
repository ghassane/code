package com.example.baserest.repositories;

import com.example.baserest.entities.Equipe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.List;
import java.util.Optional;

public interface EquipeRepository extends JpaRepository<Equipe, Integer> {
    Optional<Equipe> findByNom(String nom);
    Optional<Equipe> findFirstByPays(String pays);
    List<Equipe> findEquipesByPays(String pays);
    @Query("select count(e) from Equipe e where e.pays=:pays")
    long calculerEquipeParPays(String pays);
}

