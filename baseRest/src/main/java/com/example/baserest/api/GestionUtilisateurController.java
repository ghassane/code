package com.example.baserest.api;

import com.example.baserest.entities.UserCredentials;
import com.example.baserest.security.JwtTokenManager;
import com.example.baserest.security.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@CrossOrigin("*")
public class GestionUtilisateurController {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenManager jwtTokenManager;

    @Autowired
    public GestionUtilisateurController(UserRepository userRepository, PasswordEncoder passwordEncoder, JwtTokenManager jwtTokenManager) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.jwtTokenManager = jwtTokenManager;
    }

    @PostMapping("/login")
    public ResponseEntity<String> login(@RequestBody UserCredentials userCredentials) {
        Optional<UserDetails> userDetailsOptional = userRepository.getUser(userCredentials.getIdentifiant());
        if(userDetailsOptional.isEmpty()) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
        }
        UserDetails userDetails = userDetailsOptional.get();
        if(!passwordEncoder.matches(userCredentials.getMdp(), userDetails.getPassword())){
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
        }
        List<String> roles = userDetails.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList());
        String jwtToken = jwtTokenManager.genererToken(userDetails.getUsername(), roles);
        return ResponseEntity.ok(jwtToken);
    }

    @PostMapping("/register")
    public ResponseEntity<String> register(@RequestBody UserCredentials userCredentials) {
        userRepository.registerUser(userCredentials.getIdentifiant(), passwordEncoder.encode(userCredentials.getMdp()));
        return ResponseEntity.ok("L'utilisateur a bien était ajouté!");
    }
}
