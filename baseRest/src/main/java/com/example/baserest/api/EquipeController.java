package com.example.baserest.api;

import com.example.baserest.entities.Equipe;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@SuppressWarnings("StatementWithEmptyBody")
@RestController
@CrossOrigin("*")
@RequestMapping("/equipes")
public class EquipeController {

    @PreAuthorize("hasAuthority('admin')")
    @GetMapping
    public List<Equipe> listerTousLesEquipes() {
        //...
        var equipe1 = new Equipe();
        equipe1.setId(1);
        equipe1.setNom("R.Madrid");
        equipe1.setPays("Espagne");

        var equipe2 = new Equipe();
        equipe2.setId(2);
        equipe2.setNom("PSG");
        equipe2.setPays("France");

        return List.of(equipe1, equipe2);
    }

    @Operation(summary = "Retour une équipe par son ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Equipe est trouvé",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Equipe.class)) }),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'a pas le droit à accèder à l'équipe",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "L'équipe n'existe pas",
                    content = @Content) })
    @GetMapping("/{id}")
    public Equipe obtenirEquipeParId(@Parameter(description = "L'ID de l'équipe à cherchée") @PathVariable int id) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null && auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("admin"))) {
            //...
        }
        //...
        return new Equipe();
    }

    @PostMapping
    public Equipe creerUneEquipe(@RequestBody Equipe nouvelleEquipe) {
        //...
        return nouvelleEquipe;
    }

    @PutMapping("/{id}")
    public Equipe modifierUneEquipe(@PathVariable int id, @RequestBody Equipe equipeModifiee) {
        //...
        return equipeModifiee;
    }

    @DeleteMapping("/{id}")
    public void supprimerUneEquipe(@PathVariable int id) {
        //...
    }
}
