package com.example.baserest.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Component
public class JwtTokenFilter extends OncePerRequestFilter {

    private final JwtTokenManager jwtTokenManager;

    @Autowired
    public JwtTokenFilter(JwtTokenManager jwtTokenManager) {
        this.jwtTokenManager = jwtTokenManager;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        // Lire le header HTTP Authorization
        String header = request.getHeader(HttpHeaders.AUTHORIZATION);
        if (header == null || !header.startsWith("Bearer ")) {
            chain.doFilter(request, response);
            return; // continue au filtre suivant si pas de header HTTP Authorization
        }
        // Parser le header pour récupérer le token JWT
        String token = header.split(" ")[1].trim();
        if (!jwtTokenManager.validerToken(token)) {
            chain.doFilter(request, response);
            return; // continue au filtre suivant si le token n'est pas valide
        }
        // Recupérer les i'information du token
        String identifiant = jwtTokenManager.recupererIdentifiant(token);
        List<GrantedAuthority> roles = jwtTokenManager.recupererRoles(token);
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(identifiant, null, roles);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        chain.doFilter(request, response);
    }

}