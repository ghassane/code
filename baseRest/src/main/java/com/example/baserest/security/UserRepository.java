package com.example.baserest.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component
public class UserRepository {
    private final Map<String, UserDetails> users;

    @Autowired
    public UserRepository() {
        this.users = new HashMap<>();
        this.users.put("Admin", new User("Admin", "$2a$10$ZUA54obYzun6f498jxXz6exLy0Pw6jDbdiXi0Gcdk60R.fOxRZbo2", List.of(new SimpleGrantedAuthority("admin"))));
        this.users.put("Toto", new User("Toto", "$2a$10$hzymTIBFyP2U09V60MJGeugSDTusQ3BRsttekBlOCjXBDkqo8ukEW", List.of(new SimpleGrantedAuthority("user"))));
        // Admin --> azerty
        // Toto --> 123456
    }

    public Optional<UserDetails> getUser(String identifiant) {
        return Optional.ofNullable(users.get(identifiant));
    }

    public void registerUser(String identifiant, String mdpHashed) {
        this.users.put(identifiant, new User(identifiant, mdpHashed, List.of(new SimpleGrantedAuthority("user"))));
    }
}
