package com.example.baserest.security;


import io.jsonwebtoken.Claims;
import io.jsonwebtoken.SignatureException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtTokenManager {
    private static final String SECRET_JWT = "secretJWT";

    public String genererToken(String identifiant, List<String> roles) {
        Instant maintenant = Instant.now();
        return Jwts.builder()
                .setIssuer("MonApp")
                .setSubject(identifiant)
                .setAudience("client")
                .setIssuedAt(Date.from(maintenant))
                .setExpiration(Date.from(maintenant.plus(5, ChronoUnit.HOURS)))
                .claim("roles", roles)
                .signWith(SignatureAlgorithm.HS256, SECRET_JWT)
                .compact();
    }

    public String recupererIdentifiant(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(SECRET_JWT)
                .parseClaimsJws(token) // JWS = JWT with Signature
                .getBody();
        return claims.getSubject();
    }

    @SuppressWarnings("unchecked")
    public List<GrantedAuthority> recupererRoles(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(SECRET_JWT)
                .parseClaimsJws(token) // JWS = JWT with Signature
                .getBody();
        return convertirRole(claims.get("roles", List.class));
    }

    private List<GrantedAuthority> convertirRole(List<String> roles){
        return roles.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
    }

    public boolean validerToken(String token) {
        try {
            Claims claims = Jwts.parser()
                    .setSigningKey(SECRET_JWT)
                    .parseClaimsJws(token) // JWS = JWT with Signature
                    .getBody();
            return claims.getExpiration().after(Date.from(Instant.now()));
        } catch (SignatureException e) {
            // Si le token jwt n'a pas une signature valide
            return false;
        }
    }
}
