package com.example.baserest.api;

import com.example.baserest.entities.Equipe;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class EquipeControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void lister_tous_les_equipes() throws Exception {
        var equipe1 = new Equipe();
        var equipe2 = new Equipe();
        List<Equipe> equipesAttendue = List.of(equipe1, equipe2);
        MvcResult mvcResult = this.mockMvc.perform(get("/api/v1/equipes"))
                .andExpect(status().isOk())
                .andReturn();
        List<Equipe> equipes = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<>() {});
        Assertions.assertEquals(equipesAttendue.size(), equipes.size());
        Assertions.assertArrayEquals(equipesAttendue.toArray(new Equipe[0]), equipes.toArray(new Equipe[0]));
    }
}
