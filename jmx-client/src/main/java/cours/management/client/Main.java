package cours.management.client;

import cours.management.UserControlMBean;

import javax.management.InstanceNotFoundException;
import javax.management.MBeanServerConnection;
import javax.management.MBeanServerInvocationHandler;
import javax.management.MalformedObjectNameException;
import javax.management.Notification;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException, MalformedObjectNameException, InstanceNotFoundException, InterruptedException {
        JMXServiceURL address = new JMXServiceURL("service:jmx:rmi:///jndi/rmi://"
                + "localhost" + ":" + 9999 + "/jmxrmi");
        ObjectName objectName = new ObjectName("mon.app:type=management,categorie=user,name=control");
        try(JMXConnector connector = JMXConnectorFactory.connect(address, null)) {
            MBeanServerConnection mbsc = connector.getMBeanServerConnection();
            mbsc.addNotificationListener(objectName, Main::handleNotification, null, null);
            UserControlMBean mbean = MBeanServerInvocationHandler
                    .newProxyInstance(mbsc, objectName, UserControlMBean.class, false);

            mbean.connectUser("Test");
            System.out.println(mbean.getUsersCount());
            System.out.println(mbean.getLastUserName());
            mbean.connectUser("Toto");
            System.out.println(mbean.disconnectUser("Test"));
            System.out.println(mbean.getUsersCount());

            Thread.sleep(2000);
        }

    }

    public static void handleNotification(Notification notification, Object handback) {
        System.out.println("Notification recue:");
        System.out.println("\tClassName: " + notification.getClass().getName());
        System.out.println("\tSource: " + notification.getSource());
        System.out.println("\tType: " + notification.getType());
        System.out.println("\tMessage: " + notification.getMessage());
    }
}
