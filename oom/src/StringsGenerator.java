import java.util.ArrayList;
import java.util.List;
import java.util.Random;

class StringsGenerator {
    private static final List<String> generatedStrings = new ArrayList<>();

    String generate() {
        int leftLimit = 97; // 'a'
        int rightLimit = 122; // 'z'
        int targetStringLength = 5_000_000;
        Random random = new Random();

        String generatedString = random.ints(leftLimit, rightLimit + 1)
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();

        generatedStrings.add(generatedString);

        return generatedString;
    }
}
