package cours.management;

import javax.management.MBeanNotificationInfo;
import javax.management.Notification;
import javax.management.NotificationBroadcasterSupport;
import java.util.ArrayList;
import java.util.List;

public class UserControl extends NotificationBroadcasterSupport implements UserControlMBean {
    private final List<String> users = new ArrayList<>();

    private long notifSequenceNumber = 1;

    @Override
    public int getUsersCount() {
        return users.size();
    }

    @Override
    public String getLastUserName() {
        if(users.size() > 0) {
            return users.get(users.size() - 1);
        }
        return "";
    }

    @Override
    public void connectUser(String name) {
        users.add(name);
        Notification notif = new Notification(
                "UserConnected",
                this,
                notifSequenceNumber++,
                System.currentTimeMillis(),
                "Un utilisateur vient de se connecter: " + name
        );
        sendNotification(notif);
    }

    @Override
    public int disconnectUser(String name) {
        users.remove(name);
        try {
            return users.size();
        } finally {
            Notification notif = new Notification(
                    "UserDisconnected",
                    this,
                    notifSequenceNumber++,
                    System.currentTimeMillis(),
                    "Un utilisateur vient de se deconnecter: " + name
            );
            sendNotification(notif);
        }
    }

    @Override
    public MBeanNotificationInfo[] getNotificationInfo() {
        MBeanNotificationInfo userConnectedNotif = new MBeanNotificationInfo(
                new String[] {
                        "UserConnected"
                },
                    Notification.class.getName()
                ,
                "Un utilisateur vient de se connecter"
                );
        MBeanNotificationInfo userDisonnectedNotif = new MBeanNotificationInfo(
                new String[] {
                        "UserDisconnected"
                },
                Notification.class.getName()
                ,
                "Un utilisateur vient de se deconnecter"
        );
        return new MBeanNotificationInfo[] {userConnectedNotif, userDisonnectedNotif};
    }
}
