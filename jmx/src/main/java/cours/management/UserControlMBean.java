package cours.management;

public interface UserControlMBean {
    // Attributs
    int getUsersCount();
    String getLastUserName();

    // Opérations
    void connectUser(String name);
    int disconnectUser(String name);
}
