package org.example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

final class MaClasseTest {

    private MaClasse maClasse;

    @BeforeEach
    void createInstance() {
        maClasse = new MaClasse();
    }

    @Test
    void addition_de_deux_nombres_entiers() {
        int resultat = maClasse.addition(5, 2);
        Assertions.assertEquals(7, resultat);
    }

    @Test
    void division_de_deux_nombres_entiers() {
        double resultat = maClasse.division(4, 2);
        Assertions.assertEquals(2, resultat);
    }

    @Test
    void division_par_zero_doit_lever_une_exception() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> maClasse.division(2, 0), "On ne peut pas diviser par zéro");
    }
}
