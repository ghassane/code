package org.example;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class ClasseATesteeTest {

    @Test
    void test() {
        ClasseDependance dependanceMock = Mockito.mock(ClasseDependance.class);
        var objetATeste = new ClasseATestee(dependanceMock);
        int a = 10;
        int b = 5;
        int c = 100000;
        Mockito.when(dependanceMock.ajouter(a, b)).thenReturn(c);
        objetATeste.executer(a, b);
        Mockito.verify(dependanceMock).exporter(Mockito.eq("add"));
    }
}

