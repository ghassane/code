package org.example;

public class ClasseATestee {
    private final ClasseDependance dependance;

    public ClasseATestee(ClasseDependance dependance) {
        this.dependance = dependance;
    }

    public void executer(int a, int b) {
        int resultat = dependance.ajouter(a, b);
        dependance.exporter(String.valueOf(resultat));
    }
}
