package org.example;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

public class Main {
    public static void main(String args[]) throws Exception {
        Path src = Paths.get("e:", "BonjourJava11.java");
        Path dst = Files.createFile(Paths.get("e:", "BonjourJava11222.java"));
        if(!Files.exists(src)) {
            throw new Exception("Fichier introuvable!");
        }
        List<String> lignes = Files.readAllLines(src);
        for(String ligne: lignes) {
            Files.write(dst, (ligne + "\n").getBytes(), StandardOpenOption.APPEND);
        }
    }
}
