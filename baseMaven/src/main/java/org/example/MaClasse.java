package org.example;

public final class MaClasse {
    public int addition(int a, int b){
        return a + b;
    }

    public double division(int a, int b) {
        if (b == 0) {
            throw new IllegalArgumentException("On ne peut pas diviser par zéro");
        }
        return (double) a/b;
    }
}
