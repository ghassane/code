package com.example.jpa;

import com.example.jpa.entities.Equipe;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

class TestDB {

    @Test
    void test(){
        Equipe equipe = new Equipe();
        equipe.setNom("PSG");
        equipe.setPays("France");

        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("mon-unite-de-persistance");
        EntityManager entityManager = entityManagerFactory.createEntityManager();


        try {
            entityManager.getTransaction().begin();
            entityManager.persist(equipe);
            entityManager.flush();
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }
}
