package com.example.jpa;

import com.example.jpa.entities.Equipe;
import com.example.jpa.entities.Joueur;
import com.example.jpa.operations.DAO;
import com.example.jpa.operations.EquipeDAO;
import com.example.jpa.operations.JoueurDAO;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public final class Main {
    private static final String UNITE_DE_PERSISTANCE = "mon-unite-de-persistance";

    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory(UNITE_DE_PERSISTANCE);
        try {
            DAO<Integer, Equipe> equipeDAO = new EquipeDAO(entityManagerFactory);
            DAO<Integer, Joueur> joueurDAO = new JoueurDAO(entityManagerFactory);

            var psg = new Equipe();
            psg.setNom("PSG2");
            psg.setPays("France");
            psg = equipeDAO.create(psg);

            var mbappe = new Joueur();
            mbappe.setNom("Mbappé2");
            mbappe.setPrenom("Kylian");
            mbappe.setAge(22);
            mbappe.setEquipe(psg);
            mbappe = joueurDAO.create(mbappe);
            int a = mbappe.getId();
        } finally {
            entityManagerFactory.close();
        }
    }
}
