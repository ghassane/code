package com.example.jpa.operations;

import com.example.jpa.entities.Equipe;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public final class EquipeDAO implements DAO<Integer, Equipe> {
    private final EntityManagerFactory entityManagerFactory;

    public EquipeDAO(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    @Override
    public Equipe create(Equipe Equipe)  {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try  {
            entityManager.getTransaction().begin();
            entityManager.persist(Equipe);
            entityManager.getTransaction().commit();
            return Equipe;
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw new DAOOperationException(e);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public Equipe read(Integer id) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try  {
            return entityManager.find(Equipe.class, id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public Equipe update(Equipe Equipe) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try  {
            entityManager.getTransaction().begin();
            Equipe EquipeUpdated = entityManager.merge(Equipe);
            entityManager.getTransaction().commit();
            return EquipeUpdated;
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw new DAOOperationException(e);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void delete(Equipe Equipe)  {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try  {
            entityManager.getTransaction().begin();
            entityManager.remove(Equipe);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw new DAOOperationException(e);
        } finally {
            entityManager.close();
        }
    }
}
