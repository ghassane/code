package com.example.jpa.operations;

public final class DAOOperationException extends RuntimeException {

    public DAOOperationException(Exception cause) {
        super(cause);
    }
}
