package com.example.jpa.operations;

public interface DAO <ID, T> {
    T create(T entity);
    T read(ID id);
    T update(T entity);
    void delete(T entity);
}
