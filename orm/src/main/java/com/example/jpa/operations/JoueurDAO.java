package com.example.jpa.operations;

import com.example.jpa.entities.Joueur;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public final class JoueurDAO implements DAO<Integer, Joueur> {
    private final EntityManagerFactory entityManagerFactory;

    public JoueurDAO(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    @Override
    public Joueur create(Joueur joueur) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try  {
            entityManager.getTransaction().begin();
            entityManager.persist(joueur);
            entityManager.getTransaction().commit();
            return joueur;
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw new DAOOperationException(e);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public Joueur read(Integer id) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try  {
            return entityManager.find(Joueur.class, id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public Joueur update(Joueur joueur) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try  {
            entityManager.getTransaction().begin();
            Joueur joueurUpdated = entityManager.merge(joueur);
            entityManager.getTransaction().commit();
            return joueurUpdated;
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw new DAOOperationException(e);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void delete(Joueur joueur) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try  {
            entityManager.getTransaction().begin();
            entityManager.remove(joueur);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw new DAOOperationException(e);
        } finally {
            entityManager.close();
        }
    }
}
